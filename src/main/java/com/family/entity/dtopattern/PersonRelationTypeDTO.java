package com.family.entity.dtopattern;

import java.util.Optional;

import com.family.entity.Person;
import com.family.entity.PersonRelationType;
public class PersonRelationTypeDTO {
		
	private long id;
	private long relationType;
	private long person1;
	private long person2;

	private PersonDTO firstPersonDTO;
	private PersonDTO secondPersonDTO;
	private RelationTypeDTO theFirstPersonRelationshipToTheSecondPerson;

	public PersonRelationTypeDTO() {
	}

	public PersonRelationTypeDTO(PersonRelationType item, Person person2) {
		this.id = item.getId();
		this.firstPersonDTO = new PersonDTO(item.getFirst_person());
		this.secondPersonDTO = new PersonDTO(person2);
		this.theFirstPersonRelationshipToTheSecondPerson = new RelationTypeDTO(item.getRelationType());
	}
	


	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getRelationType() {
		return relationType;
	}

	public void setRelationType(long relationType) {
		this.relationType = relationType;
	}

	public long getPerson1() {
		return person1;
	}

	public void setPerson1(long person1) {
		this.person1 = person1;
	}

	public long getPerson2() {
		return person2;
	}

	public void setPerson2(long person2) {
		this.person2 = person2;
	}

	public PersonDTO getFirstPersonDTO() {
		return firstPersonDTO;
	}

	public void setFirstPersonDTO(PersonDTO firstPersonDTO) {
		this.firstPersonDTO = firstPersonDTO;
	}

	public PersonDTO getSecondPersonDTO() {
		return secondPersonDTO;
	}

	public void setSecondPersonDTO(PersonDTO secondPersonDTO) {
		this.secondPersonDTO = secondPersonDTO;
	}

	public RelationTypeDTO getTheFirstPersonRelationshipToTheSecondPerson() {
		return theFirstPersonRelationshipToTheSecondPerson;
	}

	public void setTheFirstPersonRelationshipToTheSecondPerson(
			RelationTypeDTO theFirstPersonRelationshipToTheSecondPerson) {
		this.theFirstPersonRelationshipToTheSecondPerson = theFirstPersonRelationshipToTheSecondPerson;
	}

}
