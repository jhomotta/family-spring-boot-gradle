package com.family.entity.dtopattern;

import com.family.entity.RelationType;

public class RelationTypeDTO {

	private Long id;
	private String name;

	public RelationTypeDTO() {
	}

	public RelationTypeDTO(RelationType item) {
		this.id = item.getId();
		this.name = item.getName();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
