package com.family.entity.dtopattern;


import com.family.entity.Person;
import com.family.entity.enums.GenderEnum;

public class PersonDTO {
	
	private Long id;
	//@NotBlank(message = "Name is mandatory")
	private String name;
	//@NotBlank(message = "Age is mandatory")
	private int age;
	//@NotBlank(message = "Gender is mandatory 'MALE' or 'FAMALE'")
	private GenderEnum gender;
	
	private String relationType;
	
	public PersonDTO() {
	}
	
	
	
	public PersonDTO(Person item) {
		this.id = item.getId();
		this.name = item.getName();
		this.age = item.getAge();
		this.gender = item.getGender();
		
	}
	
	public PersonDTO(Person item, String relationType ) {
		this.id = item.getId();
		this.name = item.getName();
		this.age = item.getAge();
		this.gender = item.getGender();
		this.relationType = relationType;
		
	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public GenderEnum getGender() {
		return gender;
	}

	public void setGender(GenderEnum gender) {
		this.gender = gender;
	}
	
}
