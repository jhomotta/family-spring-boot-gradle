package com.family.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.family.entity.dtopattern.RelationTypeDTO;

import javax.persistence.JoinColumn;

@Entity
@Table(name = "relation_type")
public class RelationType extends AuditModel implements Serializable {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String name;
	@OneToMany(mappedBy = "relationType")
    private List<PersonRelationType> personRelationType = new ArrayList<PersonRelationType>();
	
	public RelationType() {
	}
	
	public RelationType(RelationTypeDTO relationTypeDTO) {
		this.name = relationTypeDTO.getName();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	
	public List<PersonRelationType> getPersonRelationType() {
		return personRelationType;
	}

	public void setPersonRelationType(List<PersonRelationType> personRelationType) {
		this.personRelationType = personRelationType;
	}

	

	@Override
	public String toString() {
		return "RelationType [id=" + id + ", name=" + name + ", personRelationType=" + personRelationType + "]";
	}



	/**
	 * 
	 */
	private static final long serialVersionUID = 8339258620631969355L;

}
