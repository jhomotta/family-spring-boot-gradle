package com.family.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "person_relation_type")
public class PersonRelationType extends AuditModel implements Serializable {

	/**
	* 
	*/
	private static final long serialVersionUID = 7754078459688784256L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id;

	@Column(name = "second_person_id")
	private Long secondPerson_id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "first_person_id")
	Person first_person;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "relation_type_id")
	RelationType relationType;

	public PersonRelationType() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getSecondPerson_id() {
		return secondPerson_id;
	}

	public void setSecondPerson_id(Long secondPerson_id) {
		this.secondPerson_id = secondPerson_id;
	}

	public Person getFirst_person() {
		return first_person;
	}

	public void setFirst_person(Person first_person) {
		this.first_person = first_person;
	}

	public RelationType getRelationType() {
		return relationType;
	}

	public void setRelationType(RelationType relationType) {
		this.relationType = relationType;
	}

	@Override
	public String toString() {
		return "PersonRelationType [id=" + id + ", secondPerson_id=" + secondPerson_id + ", first_person="
				+ first_person + ", relationType=" + relationType + "]";
	}
	
	
}
