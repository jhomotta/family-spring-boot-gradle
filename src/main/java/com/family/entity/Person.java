package com.family.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import com.family.entity.dtopattern.PersonDTO;
import com.family.entity.enums.GenderEnum;

@Entity
@Table(name = "person")
public class Person extends AuditModel implements Serializable {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String name;
	private int age;
	private GenderEnum gender;
	private String genderName;
	
    @OneToMany(mappedBy = "first_person")
    private List<PersonRelationType> personRelationType = new ArrayList<PersonRelationType>();
	

	public Person() {
	}
	
	public Person(PersonDTO personDTO) {
		this.name = personDTO.getName();
		this.age = personDTO.getAge();
		this.gender = personDTO.getGender();
	}

	@PrePersist
	private void prePersist() {
		this.genderName = this.gender.getDescription();
	}
	
	@PreUpdate
	private void preUpdate() {
		this.genderName = this.gender.getDescription();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public GenderEnum getGender() {
		return gender;
	}

	public void setGender(GenderEnum gender) {
		this.gender = gender;
	}


	public String getGenderName() {
		return genderName;
	}

	public void setGenderName(String genderName) {
		this.genderName = genderName;
	}
	
	

	public List<PersonRelationType> getPersonRelationType() {
		return personRelationType;
	}

	public void setPersonRelationType(List<PersonRelationType> personRelationType) {
		this.personRelationType = personRelationType;
	}



	@Override
	public String toString() {
		return "Person [id=" + id + ", name=" + name + ", age=" + age + ", gender=" + gender + ", genderName="
				+ genderName + ", personRelationType=" + personRelationType + "]";
	}



	/**
	 * 
	 */
	private static final long serialVersionUID = 522444845933387972L;

}
