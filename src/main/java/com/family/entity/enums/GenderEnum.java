package com.family.entity.enums;

public enum GenderEnum {
	FEMALE("female"), MALE("male");

	private String description;

	private GenderEnum(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

}
