package com.family.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.family.entity.dtopattern.PersonDTO;
import com.family.entity.enums.GenderEnum;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class PersonNotFoundException extends RuntimeException {
	private String name;
	private int age;
	private GenderEnum gender;

	public PersonNotFoundException(PersonDTO personDTO) {
		super(String.format("%s not found with %s : '%s'", personDTO.getName(), personDTO.getAge(), personDTO.getGender()));
		this.name = personDTO.getName();
		this.age = personDTO.getAge();
		this.gender = personDTO.getGender();
	}

	public String getName() {
		return name;
	}

	public int getAge() {
		return age;
	}

	public GenderEnum getGender() {
		return gender;
	}


}
