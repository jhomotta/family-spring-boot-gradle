package com.family.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


import com.family.entity.dtopattern.RelationTypeDTO;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class RelationTypeNotFoundException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6497531016099516268L;
	private String name;
	
	public RelationTypeNotFoundException(RelationTypeDTO relationTypeDTO) {
		super(String.format("not found with: '%s'", relationTypeDTO.getName()));
		this.name = relationTypeDTO.getName();
	}

	public String getName() {
		return name;
	}

}
