package com.family.controller;

import java.util.List;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.family.entity.dtopattern.PersonRelationTypeDTO;
import com.family.service.PersonRelationTypeService;

@Controller
@RequestMapping("/relate_people")
@SessionAttributes("relate_people")
public class RelatePeopleController {

	
	@Autowired
	private PersonRelationTypeService relatePeopleService;


	@GetMapping()
	public ResponseEntity<List<PersonRelationTypeDTO>> getAllRelation() {
		return new ResponseEntity<List<PersonRelationTypeDTO>>(this.relatePeopleService.getAllRelation(), HttpStatus.CREATED);
	}		
	
	
	@PostMapping()
	public ResponseEntity<PersonRelationTypeDTO> addRelationType(@RequestBody PersonRelationTypeDTO relatePeopleDTO) {
		
		PersonRelationTypeDTO result =this.relatePeopleService.addPersonRelation(relatePeopleDTO);
		if(!result.equals(null))
			return new ResponseEntity<PersonRelationTypeDTO>(result, HttpStatus.CREATED);
		else
			return new ResponseEntity<PersonRelationTypeDTO>(HttpStatus.CONFLICT);
		
	}
}
