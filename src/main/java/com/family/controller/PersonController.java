package com.family.controller;


import java.util.List;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.family.entity.dtopattern.PersonDTO;
import com.family.service.PersonService;


@Controller
@RequestMapping("/person")
@SessionAttributes("person")
public class PersonController {
	
	@Autowired
	private PersonService personService;
	
	@GetMapping("/{id}")
	public ResponseEntity<PersonDTO> getPersonById(@PathVariable(value = "id") Long id) {
		try {
			return new ResponseEntity<PersonDTO>(this.personService.getPerson(id), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<PersonDTO>(HttpStatus.NOT_FOUND);
		}
	}
	
	@GetMapping()
	public ResponseEntity<List<PersonDTO>> getAllPeople() {
		return new ResponseEntity<List<PersonDTO>>(this.personService.getAllPeople(), HttpStatus.OK);
	}		
	
	@PostMapping()
	public ResponseEntity<PersonDTO> addPerson(@RequestBody PersonDTO personDTO) {
		try {
			return new ResponseEntity<PersonDTO>(this.personService.addPerson(personDTO), HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<PersonDTO>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<PersonDTO> updatePerson(@PathVariable(value = "id") Long id, @RequestBody PersonDTO personDTO) {

		try {
			return new ResponseEntity<PersonDTO>(this.personService.updatePerson(id, personDTO), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<PersonDTO>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<PersonDTO> deletePerson(@PathVariable(value = "id") Long id) {

		try {
			this.personService.deletePerson(id);
			return ResponseEntity.ok().build();
		} catch (Exception e) {
			return ResponseEntity.badRequest().build();
		}	
	}
}
