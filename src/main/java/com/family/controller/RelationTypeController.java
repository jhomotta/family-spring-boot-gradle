package com.family.controller;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.family.entity.dtopattern.RelationTypeDTO;
import com.family.service.RelationTypeService;

@Controller
@RequestMapping("/relation_type")
@SessionAttributes("relation_type")
public class RelationTypeController {

	@Autowired
	private RelationTypeService relationTypeService;

	@GetMapping("/{id}")
	public ResponseEntity<RelationTypeDTO> getRelationTypeById(@PathVariable(value = "id") Long id) {
		try {
			return new ResponseEntity<RelationTypeDTO>(this.relationTypeService.getRelationType(id), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<RelationTypeDTO>(HttpStatus.NOT_FOUND);
		}
	}
	
	@GetMapping()
	public ResponseEntity<List<RelationTypeDTO>> getAllRelationTypes() {
		return new ResponseEntity<List<RelationTypeDTO>>(this.relationTypeService.getAllRelationTypes(), HttpStatus.OK);
	}		
	
	@PostMapping()
	public ResponseEntity<RelationTypeDTO> addRelationType( @RequestBody RelationTypeDTO relationTypeDTO) {
		try {
			return new ResponseEntity<RelationTypeDTO>(this.relationTypeService.addRelationType(relationTypeDTO), HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<RelationTypeDTO>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<RelationTypeDTO> updateRelationType(@PathVariable(value = "id") Long id, @RequestBody RelationTypeDTO relationTypeDTO) {

		try {
			return new ResponseEntity<RelationTypeDTO>(this.relationTypeService.updateRelationType(id, relationTypeDTO), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<RelationTypeDTO>(HttpStatus.BAD_REQUEST);
		}
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<RelationTypeDTO> deleteRelationType(@PathVariable(value = "id") Long id) {

		try {
			this.relationTypeService.deleteRelationType(id);
			return ResponseEntity.ok().build();
		} catch (Exception e) {
			return ResponseEntity.badRequest().build();
		}	
	}

}
