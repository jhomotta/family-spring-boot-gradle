package com.family.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.family.dao.PersonDao;
import com.family.entity.Person;
import com.family.entity.dtopattern.PersonDTO;
import com.family.exception.PersonNotFoundException;

@Service
public class PersonService {

	@Autowired
	private PersonDao personDao;

	public PersonDTO getPerson(Long id) {
		return new PersonDTO(this.personDao.findById(id).orElseThrow());
	}

	public List<PersonDTO> getAllPeople() {
		Iterable<Person> people = this.personDao.findAll();
		List<PersonDTO> peopleDto = new ArrayList<>();

		for (Person person : people) {
			peopleDto.add(new PersonDTO(person));
		}

		return peopleDto;
	}

	public PersonDTO addPerson(PersonDTO personDTO) {
		return new PersonDTO(this.personDao.save(new Person(personDTO)));
	}

	public void deletePerson(long id) {
		
		Person person = this.personDao.findById(id)
				.orElseThrow(() -> new PersonNotFoundException(null));
		
		this.personDao.delete(person);
		
	}
	
	public PersonDTO updatePerson(Long id, PersonDTO personDTO) {
		Person person = this.personDao.findById(id).orElseThrow(() -> new PersonNotFoundException(personDTO));
		
		person.setName(personDTO.getName());
		person.setGender(personDTO.getGender());
		person.setAge(personDTO.getAge());
		
		return new PersonDTO(this.personDao.save(person));
		
	}

}
