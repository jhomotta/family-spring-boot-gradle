package com.family.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.family.dao.RelationTypeDao;
import com.family.entity.RelationType;
import com.family.entity.dtopattern.RelationTypeDTO;
import com.family.exception.RelationTypeNotFoundException;

@Service
public class RelationTypeService {

	@Autowired
	private RelationTypeDao relationTypeDao;



	public RelationTypeDTO getRelationType(Long id) {
		return new RelationTypeDTO(this.relationTypeDao.findById(id).orElseThrow());
	}

	public List<RelationTypeDTO> getAllRelationTypes() {
		Iterable<RelationType> relationTypes = this.relationTypeDao.findAll();
		List<RelationTypeDTO> relationTypeDto = new ArrayList<>();

		for (RelationType relationType : relationTypes) {
			relationTypeDto.add(new RelationTypeDTO(relationType));
		}

		return relationTypeDto;
	}

	public RelationTypeDTO addRelationType(RelationTypeDTO relationTypeDTO) {
		return new RelationTypeDTO(this.relationTypeDao.save(new RelationType(relationTypeDTO)));
	}

	public void deleteRelationType(long id) {
		
		RelationType relationType = this.relationTypeDao.findById(id)
				.orElseThrow(() -> new RelationTypeNotFoundException(null));
		
		this.relationTypeDao.delete(relationType);
		
	}
	
	public RelationTypeDTO updateRelationType(Long id, RelationTypeDTO relationTypeDTO) {
		RelationType relationType = this.relationTypeDao.findById(id)
				.orElseThrow(() -> new RelationTypeNotFoundException(relationTypeDTO));
		
		relationType.setName(relationTypeDTO.getName());
		
		
		return new RelationTypeDTO(this.relationTypeDao.save(relationType));
		
	}
}



