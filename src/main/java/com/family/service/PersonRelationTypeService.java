package com.family.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.family.dao.PersonDao;
import com.family.dao.PersonRelationTypeDao;
import com.family.dao.RelationTypeDao;
import com.family.entity.Person;
import com.family.entity.PersonRelationType;
import com.family.entity.RelationType;
import com.family.entity.dtopattern.PersonRelationTypeDTO;
import com.family.exception.PersonNotFoundException;
import com.family.exception.RelationTypeNotFoundException;

@Service
public class PersonRelationTypeService {
	
	@Autowired
	private PersonDao personDao;
	
	@Autowired
	private RelationTypeDao relationTypeDao;
	
	@Autowired
	private PersonRelationTypeDao personRelationTypeDao;
	
	
	public PersonRelationTypeDTO addPersonRelation(PersonRelationTypeDTO relatePeopleDTO) {
		
		Person person1 = this.personDao.findById(relatePeopleDTO.getPerson1()).orElseThrow(() -> new PersonNotFoundException(null));
		Person person2 = this.personDao.findById(relatePeopleDTO.getPerson2()).orElseThrow(() -> new PersonNotFoundException(null));
		
		RelationType relationType = this.relationTypeDao.findById(relatePeopleDTO.getRelationType()).orElseThrow(() -> new RelationTypeNotFoundException(null));
		
		PersonRelationType personRelationType = new PersonRelationType();
		
		personRelationType.setFirst_person(person1);
		personRelationType.setSecondPerson_id(person2.getId());
		personRelationType.setRelationType(relationType);
		
		if(this.personRelationTypeDao.CountByPeoplAndRelationType(person1.getId(), person2.getId(), relationType.getId()) > 0)
			return null;
				
		
		return new PersonRelationTypeDTO(this.personRelationTypeDao.save(personRelationType), person2);
	}
	
	public List<PersonRelationTypeDTO> getAllRelation() {
		List<PersonRelationType> relations= (List<PersonRelationType>) this.personRelationTypeDao.findAll();
		List<PersonRelationTypeDTO> relationDTO = new ArrayList<PersonRelationTypeDTO>();
		for (PersonRelationType personRelationType : relations) {
			relationDTO.add(new PersonRelationTypeDTO(personRelationType, this.personDao.findById(personRelationType.getSecondPerson_id()).orElseThrow()));
		}
		
		return relationDTO;
	}
	

}
