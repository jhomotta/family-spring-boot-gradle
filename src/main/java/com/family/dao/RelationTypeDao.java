package com.family.dao;

import org.springframework.data.repository.CrudRepository;

import com.family.entity.RelationType;


public interface RelationTypeDao extends CrudRepository<RelationType, Long>  {

}
