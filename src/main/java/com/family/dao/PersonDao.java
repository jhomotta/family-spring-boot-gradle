package com.family.dao;

import org.springframework.data.repository.CrudRepository;

import com.family.entity.Person;


public interface PersonDao extends CrudRepository<Person, Long>  {

}
