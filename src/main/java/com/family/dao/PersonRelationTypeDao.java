package com.family.dao;



import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.family.entity.PersonRelationType;

public interface PersonRelationTypeDao extends CrudRepository<PersonRelationType, Long>  {
	


	@Query(value = "SELECT COUNT(1) FROM db_springboot.person_relation_type prt WHERE prt.first_person_id=:idperson1 and prt.second_person_id=:idperson2 and prt.relation_type_id=:idRelationType", nativeQuery=true)
	public long CountByPeoplAndRelationType(@Param("idperson1") long idperson1, @Param("idperson2") long idPerson2, @Param("idRelationType") long idRelationType);
	
}
